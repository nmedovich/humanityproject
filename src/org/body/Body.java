package org.body;

public class Body {
	
	public Ear leftEar;
	public Ear rightEar;
	
	public Body(Ear leftEar, Ear rightEar){
		leftEar.setSide("left");
		rightEar.setSide("right");
	}
	
	public void performFunction(int function){
		switch(function){
		case 1:
			Ear[] ears = new Ear[2];
			ears[0] = this.leftEar;
			ears[1] = this.rightEar;
			Ear.listen(ears);
		
		default:
			System.err.println("Invalid function in org.body.Body at performFunction(int)");
		}
	}

}
