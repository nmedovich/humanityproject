package org.brain;

import org.body.Body;
import org.electrical.Connections;

public class Brain{
	
	public Body body;

	public Brain(Body body){
		this.body = body;
		Connections.estalishFunctionality();
	}
	
	void performFunction(int function){
		this.body.performFunction(function);
	}
}
